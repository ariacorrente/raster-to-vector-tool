#include "MainWindow.h"

#include "FiltersControls.h"
#include "FiltersProcessor.h"
#include "DisplayArea.h"

#include <QDebug>
#include <QLabel>
#include <QToolBar>
#include <QStatusBar>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QApplication>
#include <QScreen>

MainWindow::MainWindow (QWidget *parent) :
    QMainWindow(parent)
{
    processor = new FiltersProcessor(this);
    initGui();
}

MainWindow::~MainWindow() = default;

void MainWindow::initGui()
{
    setWindowTitle(tr("Raster to Vector Tool"));

    filtersControls = new FiltersControls(this);
    filtersControls->setProcessor(processor);

    displayArea = new DisplayArea(this);
    displayArea->setProcessor(processor);

    QAction *actionLoadImage = new QAction(tr("Load image"), this);
    connect(actionLoadImage, &QAction::triggered,
            this, &MainWindow::loadImage);

    QAction *actionSaveImage = new QAction(tr("Save image"), this);
    connect(actionSaveImage, &QAction::triggered,
            this, &MainWindow::saveImage);

    QAction *actionZoomIn = new QAction(this);
    actionZoomIn->setText(tr("Zoom in"));
    actionZoomIn->setShortcut(QKeySequence::ZoomIn);
    connect(actionZoomIn, &QAction::triggered,
            displayArea, &DisplayArea::zoomIn);

    QAction *actionZoom1 = new QAction(this);
    actionZoom1->setText(tr("Reset zoom"));
    connect(actionZoom1, &QAction::triggered,
            displayArea, &DisplayArea::zoom1);

    QAction *actionZoomOut = new QAction(this);
    actionZoomOut->setText(tr("Zoom out"));
    actionZoomOut->setShortcut(QKeySequence::ZoomOut);
    connect(actionZoomOut, &QAction::triggered,
            displayArea, &DisplayArea::zoomOut);

    QToolBar *toolbar = new QToolBar(tr("Main toolbar"), this);
    toolbar->addAction(actionLoadImage);
    toolbar->addAction(actionZoomIn);
    toolbar->addAction(actionZoom1);
    toolbar->addAction(actionZoomOut);
    toolbar->addAction(actionSaveImage);
    addToolBar(toolbar);

    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(filtersControls);
    mainLayout->addWidget(displayArea);
    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(mainLayout);
    setCentralWidget(centralWidget);

    labelZoom = new QLabel(tr("Zoom:"), this);
    connect(displayArea, &DisplayArea::zoomLevelChanged,
            this, &MainWindow::onZoomChanged);
    labelZoom->setMinimumWidth(100);

    labelResolution = new QLabel(tr("Image resolution:"), this);
    labelResolution->setMinimumWidth(200);

    QStatusBar *statusBar = new QStatusBar(this);
    statusBar->addPermanentWidget(labelZoom);
    statusBar->addPermanentWidget(labelResolution);
    setStatusBar(statusBar);

    resize(QApplication::primaryScreen()->availableSize() * 3 / 5);
}

void MainWindow::loadImage()
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this,
        tr("Load image"),
        QDir::homePath(),
        tr("Images (*.bmp *.png *.xpm *.jpg *.jpeg);;All files (*.*)"));

    if (!fileName.isEmpty())
    {
        processor->loadInputImage(fileName);
        QSize resolution;
        processor->getImageResolution(resolution);
        updateToolbarImageResolution(resolution);
        displayArea->zoom1(false);
        const Rtvt::FilterConfig filters = filtersControls->getFilters();
        processor->run(filters);
    }
}

void MainWindow::saveImage()
{
    QString fileName;
    fileName = QFileDialog::getSaveFileName(this,
        tr("Save image"),
        QDir::homePath(),
        tr("Images (*.bmp *.png *.xpm *.jpg *.jpeg);;All files (*.*)"));

    if (!fileName.isEmpty())
    {
        processor->saveImage(fileName);
    }
}

void MainWindow::onZoomChanged(float zoomLevel)
{
    labelZoom->setText(tr("Zoom: %1%").arg(zoomLevel * 100, 3));
}

void MainWindow::updateToolbarImageResolution(QSize resolution)
{
    labelResolution->setText(
        tr("Resolution: %1x%2 px")
            .arg(resolution.width())
            .arg(resolution.height()
        )
    );
}
