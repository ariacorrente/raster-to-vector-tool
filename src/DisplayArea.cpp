/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DisplayArea.h"

#include "ImageDisplay.h"
#include "FiltersProcessor.h"

#include <QDebug>
#include <QHBoxLayout>

DisplayArea::DisplayArea(QWidget* parent)
: QWidget(parent)
, zoomLevel(1.0)
, processor(nullptr)
{
    initGui();
}

void DisplayArea::initGui()
{
    ImageDisplay *inputDisplay = new ImageDisplay(defaultInputDisplayId, this);
    inputDisplay->setPhase(Rtvt::phaseStart);
    displays.insert(defaultInputDisplayId, inputDisplay);
    connect(inputDisplay, &ImageDisplay::bindToPhase,
            this, &DisplayArea::onImageDisplayChangePhase);

    ImageDisplay *outputDisplay = new ImageDisplay(defaultOutputDisplayId, this);
    outputDisplay->setPhase(Rtvt::phaseInvert);
    displays.insert(defaultOutputDisplayId, outputDisplay);
    connect(outputDisplay, &ImageDisplay::bindToPhase,
            this, &DisplayArea::onImageDisplayChangePhase);

    QHBoxLayout *displaysLayout = new QHBoxLayout();
    displaysLayout->addWidget(inputDisplay);
    displaysLayout->addWidget(outputDisplay);
    setLayout(displaysLayout);
}

void DisplayArea::setProcessor(FiltersProcessor* processor)
{
    this->processor = processor;

    ImageDisplay *inputDisplay = displays[defaultInputDisplayId];
    ImageDisplay *outputDisplay = displays[defaultOutputDisplayId];
    processor->setDisplay(inputDisplay, Rtvt::phaseStart);
    processor->setDisplay(outputDisplay, Rtvt::phaseInvert);
}

void DisplayArea::onImageDisplayChangePhase(int displayId, int phase)
{
    ImageDisplay* display = displays.value(displayId, nullptr);
    if (display != nullptr)
    {
        processor->setDisplay(display, static_cast<Rtvt::FilterPhase>(phase));
    } else {
        qDebug() << "DisplayId not found" << displayId;
    }
    processor->run();
}

void DisplayArea::zoomIn()
{
    if (zoomLevel < 0.25)
    {
        zoomLevel = 0.25;
    } else {
        zoomLevel = zoomLevel + 0.25;
    }

    QMap<int, ImageDisplay*>::iterator iter = displays.begin();
    while (iter != displays.end())
    {
        iter.value()->setZoomLevel(zoomLevel);
        ++iter;
    }

    emit zoomLevelChanged(zoomLevel);
}

void DisplayArea::zoomOut()
{
    zoomLevel = zoomLevel - 0.25;
    if (zoomLevel < 0.125 ) {
        zoomLevel = 0.125;
    }

    QMap<int, ImageDisplay*>::iterator iter = displays.begin();
    while (iter != displays.end())
    {
        iter.value()->setZoomLevel(zoomLevel);
        ++iter;
    }

    emit zoomLevelChanged(zoomLevel);
}

void DisplayArea::zoom1(bool mustRepaint)
{
    zoomLevel = 1;

    QMap<int, ImageDisplay*>::iterator iter = displays.begin();
    while (iter != displays.end())
    {
        iter.value()->setZoomLevel(zoomLevel, mustRepaint);
        ++iter;
    }

    emit zoomLevelChanged(zoomLevel);
}
