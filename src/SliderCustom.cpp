/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SliderCustom.h"

#include <QSpinBox>
#include <QSlider>
#include <QHBoxLayout>

SliderCustom::SliderCustom(QWidget *parent)
: QWidget(parent),
oddOnly(false)
{
    initGui();
}

void SliderCustom::initGui()
{
    spinbox = new QSpinBox(this);
    connect(spinbox, QOverload<int>::of(&QSpinBox::valueChanged),
            this, &SliderCustom::onValueChanged);

    slider = new QSlider(this);
    slider->setOrientation(Qt::Horizontal);
    connect(slider, &QSlider::valueChanged,
            this, &SliderCustom::onValueChanged);

    setTooltip("");
    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(spinbox);
    layout->addWidget(slider);
    setLayout(layout);
}

void SliderCustom::setRange(int min, int max)
{
    spinbox->setRange(min, max);
    slider->setRange(min, max);
    updateTooltip();
}

void SliderCustom::setOddOnly(bool oddOnly)
{
    this->oddOnly = oddOnly;
    updateTooltip();
}

void SliderCustom::setValue(int value)
{
    spinbox->setValue(value);
    slider->setValue(value);
}

void SliderCustom::setTooltip(const QString& text)
{
    tooltipMessage = text;
    updateTooltip();
}

void SliderCustom::updateTooltip()
{
    QString rangeText = QString(tr("range %1 to %2")
        .arg(slider->minimum())
        .arg(slider->maximum()));
    QString oddOnlyText = QString();
    if (oddOnly)
    {
        oddOnlyText = tr("odd values only");
    } else {
        oddOnlyText = tr("all values");
    }

    QString fullText = QString("%1 (%2, %3)")
        .arg(tooltipMessage)
        .arg(rangeText)
        .arg(oddOnlyText);
    spinbox->setToolTip(fullText);
    slider->setToolTip(fullText);
}


void SliderCustom::onValueChanged(int value)
{
    int fixedValue = value;
    if (oddOnly && (value %2 == 0))
    {
        fixedValue = value + 1;
    }

    if (spinbox->value() != fixedValue)
    {
        spinbox->setValue(fixedValue);
    }

    if (slider->value() != fixedValue)
    {
        slider->setValue(fixedValue);
    }

    emit valueChanged(fixedValue);
}
