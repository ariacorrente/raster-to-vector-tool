/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILTERSCONTROLS_H
#define FILTERSCONTROLS_H

#include "Rtvt.h"

#include <QWidget>

class FiltersProcessor;

class FiltersControls : public QWidget
{
    Q_OBJECT

public:
    explicit FiltersControls(QWidget *parent = nullptr);
    ~FiltersControls() override;
    void setProcessor(FiltersProcessor* newProcessor) {processor = newProcessor;};
    const Rtvt::FilterConfig getFilters() const { return filter; };

public slots:
    void onSmoothChanged(int newValue);
    void onEqualizeChanged(int state);
    void onCannyLowChanged(int newValue);
    void onCannyHighChanged(int newValue);
    void onCannySizeChanged(int newValue);
    void onInvertChanged(int state);

private:
    Rtvt::FilterConfig filter;
    FiltersProcessor* processor;
    void initGui();
    void executeFilters();
};

#endif // FILTERSCONTROLS_H
