/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QVBoxLayout>
#include <QDebug>
#include <QComboBox>
#include <QScrollArea>
#include <QAction>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "Rtvt.h"
#include "ImageCanvas.h"
#include "ImageDisplay.h"

ImageDisplay::ImageDisplay(QWidget* parent)
: QWidget(parent),
displayId(0)
{
    initGui();
}

ImageDisplay::ImageDisplay(int id, QWidget* parent)
: QWidget(parent),
displayId(id)
{
    initGui();
}

void ImageDisplay::initGui()
{
    cvImage = new cv::Mat();

    comboboxPhase = new QComboBox(this);
    comboboxPhase->addItem(tr("Original image"), Rtvt::phaseStart);
    comboboxPhase->addItem(tr("Single channel"), Rtvt::phaseOneChannel);
    comboboxPhase->addItem(tr("Equalized"), Rtvt::phaseEqualize);
    comboboxPhase->addItem(tr("Smoothed image"), Rtvt::phaseSmooth);
    comboboxPhase->addItem(tr("Edge detected"), Rtvt::phaseCanny);
    comboboxPhase->addItem(tr("Image inverted"), Rtvt::phaseInvert);
    connect(comboboxPhase, QOverload<int>::of(&QComboBox::activated),
            this, &ImageDisplay::onComboBoxPhaseActivated);


    canvas = new ImageCanvas(this);

    QScrollArea *scrollArea = new QScrollArea(this);
    scrollArea->setWidget(canvas);

    QLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(comboboxPhase);
    mainLayout->addWidget(scrollArea);

    setLayout(mainLayout);
}

void ImageDisplay::show(const cv::Mat &image)
{
    QImage::Format qtFormat = QImage::Format_Grayscale8;
    if (!image.empty()) {
        if (image.type() == CV_8UC3)
        {
            // Fix channel order
            cv::cvtColor(image, *cvImage, cv::COLOR_BGR2RGB);
            qtFormat = QImage::Format_RGB888;
        } else if (image.type() == CV_8UC1)
        {
            *cvImage = image;
            qtFormat = QImage::Format_Grayscale8;
        } else {
            qDebug() << "Unsupported OpenCV image format:" << image.type();
        }

        QImage qImage = QImage((const unsigned char*)(cvImage->data),
            cvImage->cols, cvImage->rows, cvImage->step,
                               qtFormat);
        canvas->setPixmap(QPixmap::fromImage(qImage));
    }
}

void ImageDisplay::onComboBoxPhaseActivated(int index)
{
    int phase = comboboxPhase->currentData().toInt();
    emit bindToPhase(displayId, phase);
}

/**
 * Load the associated filter phase inside the combobox.
 */
void ImageDisplay::setPhase(Rtvt::FilterPhase phase)
{
    int index = comboboxPhase->findData(phase);
    comboboxPhase->setCurrentIndex(index);
}

void ImageDisplay::setZoomLevel(float zoomLevel, bool mustRepaint)
{
    canvas->setScale(zoomLevel);
    if (mustRepaint)
    {
        canvas->repaint();
    }
}
