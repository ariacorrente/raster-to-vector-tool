/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGECANVAS_H
#define IMAGECANVAS_H

#include <QWidget>
#include <QPixmap>

class ImageCanvas : public QWidget
{
    Q_OBJECT

public:
    ImageCanvas(QWidget *parent = nullptr);
    ~ImageCanvas();
    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;
    void setPixmap(const QPixmap &image);
    void setScale(float zoom);

protected:
    void paintEvent(QPaintEvent* event) override;

private:
    float scale;
    QPixmap pixmap;
    void updateGeometry();
};

#endif // IMAGECANVAS_H
