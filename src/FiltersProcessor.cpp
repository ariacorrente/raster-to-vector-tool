/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "FiltersProcessor.h"
#include "ImageDisplay.h"

FiltersProcessor::FiltersProcessor(QObject* parent)
: QObject(parent)
{
}

FiltersProcessor::~FiltersProcessor()
{

}

void FiltersProcessor::loadInputImage(const QString &filePath)
{
    inputImage = cv::imread(filePath.toStdString(), cv::IMREAD_COLOR);
    displayPhase( Rtvt::phaseStart, inputImage);
}

void FiltersProcessor::run(const Rtvt::FilterConfig &config)
{
    this->config = config;
    if (!inputImage.empty())
    {
        displayPhase( Rtvt::phaseStart, inputImage);

        cv::cvtColor(inputImage, outputImage, cv::COLOR_BGR2GRAY);
        displayPhase( Rtvt::phaseOneChannel, outputImage);

        runEqualize(config.mustEqualize);
        displayPhase( Rtvt::phaseEqualize, outputImage);

        runSmooth(config.smoothValue);
        displayPhase( Rtvt::phaseSmooth, outputImage);

        runCanny(config.cannyThresholdLow, config.cannyThresholdHigh, config.cannySize);
        displayPhase( Rtvt::phaseCanny, outputImage);

        runInvert(config.mustInvert);
        displayPhase(Rtvt::phaseInvert, outputImage);
    }
}

/**
 * Run filter using last configuration.
 */
void FiltersProcessor::run()
{
    run(config);
}

void FiltersProcessor::runSmooth(int smoothValue)
{
    if (smoothValue % 2 != 1) {
        smoothValue++;
    }
    cv::Size ksize = cv::Size(smoothValue, smoothValue);
    cv::GaussianBlur(outputImage, outputImage, ksize, 0);
}

void FiltersProcessor::runEqualize(bool mustEqualize)
{
    if (mustEqualize)
    {
        cv::equalizeHist(outputImage, outputImage);
    }
}

void FiltersProcessor::runCanny(int thresholdLow, int thresholdHigh, int size)
{
    cv::Canny(outputImage, outputImage, thresholdLow, thresholdHigh, size);
}

void FiltersProcessor::runInvert(bool mustInvert)
{
    if (mustInvert)
    {
        outputImage = 255 - outputImage;
    }
}

const cv::Mat & FiltersProcessor::getInputImage()
{
    return inputImage;
}

const cv::Mat & FiltersProcessor::getOutputImage()
{
    return outputImage;
}

void FiltersProcessor::setDisplay(ImageDisplay* display, Rtvt::FilterPhase phase)
{
    QMultiMap<Rtvt::FilterPhase, ImageDisplay*>::iterator iter = displays.begin();
    while (iter != displays.end())
    {
        if (iter.value() == display)
        {
            iter = displays.erase(iter);
        } else {
            ++iter;
        }
    }
    displays.insert(phase, display);
}

void FiltersProcessor::displayPhase( Rtvt::FilterPhase phase, const cv::Mat &image)
{
    QMultiMap<Rtvt::FilterPhase, ImageDisplay*>::iterator iter = displays.begin();
    while (iter != displays.end())
    {
        if (iter.key() == phase)
        {
            iter.value()->show(image);
        }
        ++iter;
    }
}

void FiltersProcessor::saveImage(const QString& filePath) const
{
    cv::imwrite(filePath.toStdString(), outputImage);
}

void FiltersProcessor::getImageResolution(QSize &resolution)
{
    resolution.setWidth(inputImage.cols);
    resolution.setHeight(inputImage.rows);
}
