/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DISPLAYAREA_H
#define DISPLAYAREA_H

#include <QWidget>
#include <QMap>

class ImageDisplay;
class FiltersProcessor;

class DisplayArea : public QWidget
{
    Q_OBJECT

public:
    DisplayArea(QWidget* parent = nullptr);
    void setProcessor(FiltersProcessor *processor);

public slots:
    void onImageDisplayChangePhase(int displayId, int phase);
    void zoomIn();
    void zoomOut();
    void zoom1(bool mustRepaint = true);

signals:
    void zoomLevelChanged(float zoomLevel);

private:
    const int defaultInputDisplayId = 1;
    const int defaultOutputDisplayId = 2;
    float zoomLevel;
    FiltersProcessor *processor;
    QMap<int, ImageDisplay*> displays;
    void initGui();
};

#endif // DISPLAYAREA_H
