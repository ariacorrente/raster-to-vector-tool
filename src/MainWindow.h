#ifndef VOMMAINWINDOW_H
#define VOMMAINWINDOW_H

#include <QMainWindow>

class FiltersProcessor;
class FiltersControls;
class DisplayArea;
class QLabel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow (QWidget *parent = nullptr);
    ~MainWindow() override;

public slots:
    void onZoomChanged(float zoomLevel);

private:
    DisplayArea *displayArea;
    FiltersProcessor *processor;
    FiltersControls *filtersControls;
    QPixmap* pixmapInput;
    QLabel *labelZoom;
    QLabel *labelResolution;

    void initGui();
    void loadImage();
    void saveImage();
    void updateToolbarImageResolution(QSize resolution);
};

#endif // VOMMAINWINDOW_H
