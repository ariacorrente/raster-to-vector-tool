/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMAGEDISPLAY_H
#define IMAGEDISPLAY_H

#include "Rtvt.h"

#include <QWidget>

class ImageCanvas;

namespace cv {
    class Mat;
}

class QImage;
class QPixmap;
class QComboBox;

class ImageDisplay : public QWidget
{
    Q_OBJECT

public:
    ImageDisplay(QWidget* parent = nullptr);
    ImageDisplay(int id, QWidget* parent = nullptr);
    void show(const cv::Mat &image);
    void setPhase( Rtvt::FilterPhase phase);
    void setZoomLevel(float zoomLevel, bool mustRepaint = true);

public slots:
    void onComboBoxPhaseActivated(int index);

signals:
    void bindToPhase(int displayId, int phase);

private:
    int displayId;
    ImageCanvas *canvas;
    QComboBox *comboboxPhase;
    QImage *qImage;
    QPixmap *pixmap;
    cv::Mat *cvImage;
    void initGui();
};

#endif // IMAGEDISPLAY_H
