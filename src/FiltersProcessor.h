/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILTERSPROCESSOR_H
#define FILTERSPROCESSOR_H

#include "Rtvt.h"

#include <QObject>
#include <QMultiMap>
#include <opencv2/core.hpp>

class ImageDisplay;

class FiltersProcessor : QObject
{
    Q_OBJECT

public:
    ~FiltersProcessor();
    FiltersProcessor(QObject* parent = nullptr);
    void loadInputImage(const QString &filePath);
    void run(const Rtvt::FilterConfig &config);
    void run();
    const cv::Mat& getInputImage();
    const cv::Mat& getOutputImage();
    void setDisplay(ImageDisplay *display, Rtvt::FilterPhase phase);
    void saveImage(const QString &filePath) const;
    void getImageResolution(QSize &resolution);

private:
    Rtvt::FilterConfig config;
    cv::Mat outputImage;
    cv::Mat inputImage;
    QMultiMap<Rtvt::FilterPhase, ImageDisplay*> displays;
    void runSmooth(int smoothValue);
    void runCanny(int thresholdLow, int thresholdHigh, int size);
    void runEqualize(bool mustEqualize);
    void runInvert(bool mustInvert);
    void displayPhase( Rtvt::FilterPhase phase, const cv::Mat &image);
};

#endif // FILTERSPROCESSOR_H
