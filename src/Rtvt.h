/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VECTOROMATIC_H
#define VECTOROMATIC_H

class Rtvt
{
public:
    enum FilterPhase {
        phaseStart,      // Original image
        phaseOneChannel, // From 3 channels to 1 channel
        phaseSmooth,     // Gaussian blur
        phaseEqualize,   // Histogram equalization
        phaseCanny,      // Canny edge detection
        phaseInvert      // Invert image to get black lines on white background
    };

    struct FilterConfig {
        int smoothValue;
        bool mustEqualize;
        int cannyThresholdLow;
        int cannyThresholdHigh;
        int cannySize;
        bool mustInvert;
    };
};

#endif // VECTOROMATIC_H
