/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef  SLIDERCUSTOM_H
#define  SLIDERCUSTOM_H

#include <QWidget>

class QSpinBox;
class QSlider;

class SliderCustom : public QWidget
{
    Q_OBJECT

public:
    SliderCustom(QWidget *parent = nullptr);
    void setRange(int min, int max);
    void setValue(int value);
    void setOddOnly(bool oddOnly);
    void setTooltip(const QString &text);

signals:
    void valueChanged(int value);

private:
    bool oddOnly;
    QString tooltipMessage;
    QSpinBox *spinbox;
    QSlider *slider;
    void initGui();
    void onValueChanged(int value);
    void updateTooltip();

};

#endif //  SLIDERCUSTOM_H
