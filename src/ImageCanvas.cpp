/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "ImageCanvas.h"

#include <QPainter>

ImageCanvas::ImageCanvas(QWidget *parent)
: QWidget(parent)
, scale(1)
{

}

ImageCanvas::~ImageCanvas()
{

}

QSize ImageCanvas::minimumSizeHint() const
{
    return QSize(0, 0);
}

QSize ImageCanvas::sizeHint() const
{
    return QSize(0, 0);
}

void ImageCanvas::paintEvent(QPaintEvent* event)
{
    QPainter painter(this);
    QRectF rect = pixmap.rect();

    painter.save();
    painter.scale(scale, scale);
    painter.drawPixmap(rect, pixmap, rect);
    painter.restore();
}

void ImageCanvas::setPixmap(const QPixmap& image)
{
    pixmap = image.copy();
    updateGeometry();
    repaint();
}

void ImageCanvas::setScale(float zoom)
{
    scale = zoom;
    updateGeometry();
}

void ImageCanvas::updateGeometry()
{
    int currentWidth = geometry().width();
    int currentHeight = geometry().height();
    int targetWidth = pixmap.rect().width() * scale;
    int targetHeight = pixmap.rect().height() * scale;
    if (targetWidth != currentWidth || targetHeight != currentHeight)
    {
        QRect target;
        target.setWidth(targetWidth);
        target.setHeight(targetHeight);
        setGeometry(target);
    }
}
