/*
 * Raster To Vector Tool
 * Copyright (C) 2018  Nicola Felice <dev@dominiofelice.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QVBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QDebug>
#include <QFormLayout>
#include <QSpinBox>
#include <QCheckBox>
#include <QGroupBox>

#include "ImageDisplay.h"
#include "SliderCustom.h"
#include "FiltersProcessor.h"

#include "FiltersControls.h"

FiltersControls::FiltersControls(QWidget* parent)
: QWidget(parent)
{
    filter.smoothValue = 3;
    filter.mustEqualize = false;
    filter.cannyThresholdLow = 50;
    filter.cannyThresholdHigh = 100;
    filter.cannySize = 3;
    filter.mustInvert = true;
    initGui();
}

FiltersControls::~FiltersControls()
{
}

void FiltersControls::initGui()
{
    // Preprocess filters
    SliderCustom *sliderSmooth = new SliderCustom(this);
    sliderSmooth->setRange(1, 49);
    sliderSmooth->setValue(filter.smoothValue);
    sliderSmooth->setOddOnly(true);
    connect(sliderSmooth, &SliderCustom::valueChanged,
            this, &FiltersControls::onSmoothChanged);

    QCheckBox *checkboxEqualize = new QCheckBox(this);
    checkboxEqualize->setChecked(filter.mustEqualize);
    connect(checkboxEqualize, &QCheckBox::stateChanged,
            this, &FiltersControls::onEqualizeChanged);

    QFormLayout *layoutPreprocess = new QFormLayout();
    layoutPreprocess->addRow(tr("Smooth:"), sliderSmooth);
    layoutPreprocess->addRow(tr("Equalize"), checkboxEqualize);

    QGroupBox *groupboxPreprocess = new QGroupBox(tr("Preprocess"), this);
    groupboxPreprocess->setLayout(layoutPreprocess);

    // Edge detection filters
    SliderCustom *sliderCannyLow = new SliderCustom(this);
    sliderCannyLow->setRange(1, 255);
    sliderCannyLow->setValue(filter.cannyThresholdLow);
    connect(sliderCannyLow, &SliderCustom::valueChanged,
            this, &FiltersControls::onCannyLowChanged);

    SliderCustom *sliderCannyHigh = new SliderCustom(this);
    sliderCannyHigh->setRange(1, 255);
    sliderCannyHigh->setValue(filter.cannyThresholdHigh);
    connect(sliderCannyHigh, &SliderCustom::valueChanged,
            this, &FiltersControls::onCannyHighChanged);

    SliderCustom *sliderCannySize = new SliderCustom(this);
    // Range between 3 and 7 required by OpenCV
    sliderCannySize->setRange(3, 7);
    sliderCannySize->setOddOnly(true);
    sliderCannySize->setValue(filter.cannySize);
    connect(sliderCannySize, &SliderCustom::valueChanged,
            this, &FiltersControls::onCannySizeChanged);

    QFormLayout *layoutEdgeDetection = new QFormLayout();
    layoutEdgeDetection->addRow(tr("Canny low threshold:"), sliderCannyLow);
    layoutEdgeDetection->addRow(tr("Canny high threshold:"), sliderCannyHigh);
    layoutEdgeDetection->addRow(tr("Canny size:"), sliderCannySize);

    QGroupBox *groupboxEdgeDetection = new QGroupBox(tr("Edge detection"), this);
    groupboxEdgeDetection->setLayout(layoutEdgeDetection);

    // Postprocess filters
    QCheckBox *checkboxInvert = new QCheckBox(this);
    checkboxInvert->setChecked(filter.mustInvert);
    connect(checkboxInvert, &QCheckBox::stateChanged,
            this, &FiltersControls::onInvertChanged);

    QFormLayout *layoutPostprocess= new QFormLayout();
    layoutPostprocess->addRow(tr("Invert image"), checkboxInvert);

    QGroupBox *groupboxPostprocess = new QGroupBox(tr("Postprocess"), this);
    groupboxPostprocess->setLayout(layoutPostprocess);

    // Assemble groupboxes in main layout
    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addWidget(groupboxPreprocess);
    mainLayout->addWidget(groupboxEdgeDetection);
    mainLayout->addWidget(groupboxPostprocess);

    setLayout(mainLayout);
}

void FiltersControls::onEqualizeChanged(int state)
{
    filter.mustEqualize = static_cast<Qt::CheckState>(state) == Qt::Checked;
    executeFilters();
}

void FiltersControls::onSmoothChanged(int newValue)
{
    filter.smoothValue = newValue;
    executeFilters();
}

void FiltersControls::onCannyLowChanged(int newValue)
{
    filter.cannyThresholdLow = newValue;
    executeFilters();
}

void FiltersControls::onCannyHighChanged(int newValue)
{
    filter.cannyThresholdHigh= newValue;
    executeFilters();
}

void FiltersControls::onCannySizeChanged(int newValue)
{
    filter.cannySize = newValue;
    executeFilters();
}

void FiltersControls::onInvertChanged(int state)
{
    filter.mustInvert = static_cast<Qt::CheckState>(state) == Qt::Checked;
    executeFilters();
}

void FiltersControls::executeFilters()
{
    if (processor != nullptr)
    {
        processor->run(filter);
    }
}
