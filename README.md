# Raster To Vector Tool

Tool to translate a raster image into a vectorial image.

Libraries / Frameworks used:

- OpenCV
- Qt5

## Features implemented

- filters to translate from an RGB image to a black and white bitmap of the
    borders.

## Todo

- integrate potrace/autotrace
- add more filters
