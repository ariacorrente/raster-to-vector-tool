cmake_minimum_required(VERSION 3.0)
project(RasterToVectorTool VERSION 0.1 LANGUAGES CXX)

include(GNUInstallDirs)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

find_package(Qt5 COMPONENTS Core Gui Widgets REQUIRED)
find_package(OpenCV COMPONENTS core imgproc imgcodecs  REQUIRED)

message( STATUS "Qt5 version:    ${Qt5Core_VERSION}")
message( STATUS "OpenCV version: ${OpenCV_VERSION}")

add_executable(rtvt
  src/Rtvt.cpp
  src/main.cpp
  src/MainWindow.cpp
  src/FiltersControls.cpp
  src/DisplayArea.cpp
  src/ImageDisplay.cpp
  src/ImageCanvas.cpp
  src/FiltersProcessor.cpp
  src/SliderCustom.cpp
)

if (WIN32)
    # Tell MS Windows this is a GUI application so avoid to display an empty
    # console window near the main window.
    set_property(TARGET rtvt PROPERTY WIN32_EXECUTABLE true)
endif (WIN32)

target_link_libraries(rtvt
    Qt5::Core Qt5::Gui Qt5::Widgets
    ${OpenCV_LIBS}
)

# Install the executable
install(TARGETS rtvt
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
